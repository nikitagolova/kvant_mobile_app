const separator = ':';

const responceWithoutErrors = {key: '00', message: ''};

const startResponceErrors = {
  1: {key: '01', message: 'SD карта отсутствует'},
  2: {key: '02', message: 'Ошибка монтирования файловой системы'},
  4: {key: '04', message: 'SD карта переполнена'},
  8: {key: '08', message: 'ЭКГ файл не создан'},
  16: {key: '10', message: 'Инерцеальный файл не создан'},
  32: {key: '20', message: 'Низкий заряд батареи'},
  64: {key: '40', message: ''},
  128: {
    key: '80',
    message: 'Ошибка начала записи вызвана проблемой с датчиками',
  },
};

const stopResponceErrors = {
  1: {key: '01', message: 'SD карта отсутствует'},
  2: {
    key: '02',
    message: 'Запись остановлена, но ЭКГ файл не был закрыт корректно',
  },
  4: {
    key: '04',
    message: 'Запись остановлена, но Инерциальный файл не был закрыт корректно',
  },
  8: {key: '08', message: 'Запись уже остановлена'},
};

const batteryResponceErrors = {
  1: {key: '01', message: 'Отсутствует датчик мониторинга аккумулятора'},
  2: {key: '02', message: 'Батарея отстутствует или повреждена'},
};

const heartRateResponceErrors = {
  1: {key: '01', message: 'Не работает датчик ЭКГ сигналов'},
  2: {key: '02', message: 'Не подключен красный электрод'},
  4: {key: '04', message: 'Не подключен зеленый электрод'},
  8: {key: '08', message: 'Не подключен белый электрод'},
};

const cardResponceErrors = {
  1: {key: '01', message: 'SD карта отсутствует'},
  2: {key: '02', message: 'Ошибка монтирования файловой системы'},
  4: {key: '04', message: 'SD карта переполнена'},
};

const timeResponceErrors = {
  1: {key: '01', message: 'Время не установлено'},
};

const statusResponce = {
  1: {key: '01', message: 'Идет запись на карту памяти'},
  2: {key: '02', message: 'Зарядка подключена'},
  4: {key: '04', message: 'Батарея заряжается'},
};

const responceErrors = {
  Start: startResponceErrors,
  Stop: stopResponceErrors,
  Batt: batteryResponceErrors,
  HR: heartRateResponceErrors,
  Card: cardResponceErrors,
  Time: timeResponceErrors,
  Status: statusResponce,
};

const hexToBin = (hexValue) => {
  let decValue = parseInt(hexValue, 16);
  let binValue = decValue.toString(2);
  let result = binValue.split('').reverse();

  return result;
};

export const requestCommandsEnum = {
  start: 'Start',
  stop: 'Stop',
  battery: 'GetBatt',
  heartRate: 'GetHR',
  card: 'GetCard',
  status: 'GetStatus',
  time: 'SetTime' + separator,
};

export const responceCommandsEnum = {
  start: 'Start',
  stop: 'Stop',
  battery: 'Batt',
  heartRate: 'HR',
  card: 'Card',
  status: 'Status',
  time: 'Time',
};

export const parse = (request) => {
  dataRequest = request.split(separator);

  if (dataRequest.length < 2) {
    throw 'Проблемы с парсингом пришедшей команды' + request;
  }

  let commandName = dataRequest[0];
  let commandKey = dataRequest[1];

  let result = {command: commandName, data: []};

  if (commandKey == responceWithoutErrors.key) {
    let answer = {...responceWithoutErrors, value: 0};

    if (
      commandName == responceCommandsEnum.card ||
      commandName == responceCommandsEnum.battery ||
      commandName == responceCommandsEnum.heartRate
    ) {
      if (dataRequest.length < 3) {
        throw 'Проблемы с парсингом пришедшей команды' + request;
      }

      let value = parseInt(dataRequest[2], 16);
      answer.value = value;
    }

    result.data.push(answer);
  } else {
    hexToBin(commandKey).map((value, key) => {
      let responceCommandErrors = responceErrors[commandName];
      let responceCommandKey = Math.pow(2, key);

      if (!(responceCommandKey in responceCommandErrors)) {
        return;
      }

      if (commandName == responceCommandsEnum.status) {
        result.data.push({
          ...responceCommandErrors[responceCommandKey],
          value: parseInt(value),
        });
        return;
      }

      if (value == '0') {
        return;
      }

      result.data.push({
        ...responceCommandErrors[responceCommandKey],
        value: 0,
      });
    });
  }

  console.log('Result', result);
  return result;
};
