import {BleManager} from 'react-native-ble-plx';
import Base64 from 'react-native-base64';
import {parse} from './RequestHelper';
import {requestPermission} from './PermissionsHelper';

const _manager = new BleManager();
const _prefixUUID = '0000140';
const _suffixUUID = '-1212-efde-1523-785feabcd123';

const _getServiceOrCharacteristicUUID = (num) => {
  let result = _prefixUUID + num + _suffixUUID;
  return result;
};

const _serviceUUID = _getServiceOrCharacteristicUUID(0);
const _writeCharacteristicUUID = _getServiceOrCharacteristicUUID(1);
const _readCharacteristicUUID = _getServiceOrCharacteristicUUID(2);

let subscriptionMonitorCharacteristic = null;

export class BleHelper {
  // Включение bluetooth модуля.
  static enableBluetoothModule = async (resultCallback) => {
    // Проверяем, что у приложения имеется разешение запрашивать геопозицию.
    await requestPermission();

    const subscription = _manager.onStateChange((state) => {
      console.log('onStateChange: ', state);

      //Пока будет идеальный вариант, что пользователь включит Bluetooth.
      if (state === 'PoweredOn') {
        resultCallback(true);
        subscription.remove();
      } else {
        resultCallback(false);
      }
    }, true);

    let state = await _manager.state();
    switch (state) {
      case 'PoweredOn':
        resultCallback(true);
        subscription.remove();
        break;
      case 'PoweredOff':
        _manager.enable();
        break;
    }
  };

  // Начать поиск устройств.
  static startScanDevices = (addDeviceCallBack) => {
    const subscription = _manager.onStateChange((state) => {
      if (state === 'PoweredOn') {
        _manager.startDeviceScan(null, null, (error, device) => {
          if (error) {
            console.error(error.message);
            return;
          }

          if (!device || !device.name || !device.name.includes('Kardic')) {
            return;
          }

          addDeviceCallBack(device);
        });
      }

      subscription.remove();
    }, true);
  };

  // Остановить поиск устройств.
  static stopScanDevices = () => {
    _manager.stopDeviceScan();
  };

  static deviceConnect = async (
    deviceId,
    connectedCallBack,
    disconnectedCallback,
  ) => {
    let connectedDevice = null;
    try {
      connectedDevice = await _manager.connectToDevice(deviceId, {
        timeout: 5000,
        autoConnect: false,
      });
    } catch (e) {
      let errorMessage = 'Проблемы с подключением';
      console.log(errorMessage);

      await connectedCallBack(errorMessage);
      return;
    }

    if (!connectedDevice) {
      let errorMessage = 'Устройство не было подключено! Повторите попытку.';

      await connectedCallBack(errorMessage);
      return;
    }

    await connectedDevice.discoverAllServicesAndCharacteristics();
    await connectedCallBack();

    const subscription = _manager.onDeviceDisconnected(deviceId, (error) => {
      subscription.remove();

      if (error) {
        disconnectedCallback(error.message);
        return;
      }

      disconnectedCallback();
      console.log('Сработало событие отключения устройства!');
    });
  };

  static deviceDisconnect = async (deviceId) => {
    await _manager.cancelDeviceConnection(deviceId);
  };

  // Подписка на мониторинг ответа от устройства на запрос.
  static subscribeMonitorCharacteristic = (deviceId, responceCallback) => {
    subscriptionMonitorCharacteristic = _manager.monitorCharacteristicForDevice(
      deviceId,
      _serviceUUID,
      _readCharacteristicUUID,
      (error, characteristic) => {
        if (error) {
          console.log('Monitor characteristic', error.message);
          return;
        }

        let responce = Base64.decode(characteristic.value);
        try {
          let parsedResponce = parse(responce);
          responceCallback(parsedResponce);
        } catch (e) {
          console.log(e.message);
        }
      },
    );
  };

  // Отписка от мониторинга ответа от устройства на запрос.
  static unsubscribeMonitorCharacteristic = () => {
    if (!subscriptionMonitorCharacteristic) {
      return;
    }

    subscriptionMonitorCharacteristic.remove();
    subscriptionMonitorCharacteristic = null;
  };

  // Отправка запроса на устройство.
  static writeDataToCharacteristic = (value, deviceId) => {
    let valueToBase64 = Base64.encode(value);

    _manager.writeCharacteristicWithResponseForDevice(
      deviceId,
      _serviceUUID,
      _writeCharacteristicUUID,
      valueToBase64,
    );
  };
}
