import {PermissionsAndroid} from 'react-native';

export const requestPermission = async () => {
  const test = async (permission) => {
    const tester = await PermissionsAndroid.request(permission, {
      title: 'Разрешение приложению использовать геопозицию',
      message:
        'Для дальнейшего использования приложения ' +
        'необходимо разрешить запрашивать геопозицию.',
      buttonNegative: 'Отменить',
      buttonPositive: 'Принять',
    });

    if (tester === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('Разрешение на запрос геопозиции получено.');
    } else {
      console.log('Нет разрешения на запрос геопозиции');
    }
  };

  try {
    await test(PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION);
    await test(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    await test(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  } catch (err) {
    console.warn(err);
  }
};
