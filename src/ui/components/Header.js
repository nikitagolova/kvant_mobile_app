import React, {useContext, useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity} from 'react-native';
import {
  Header,
  Body,
  Left,
  Right,
  Button,
  Icon,
  Title,
  Spinner,
} from 'native-base';
import {BatteryIconComponent} from './BatteryIcon';
import {Context} from '../../helpers/index';

const HeaderComponent = ({leftButton, title}) => {
  const {selectedDevice, typeIcon} = useContext(Context);
  const {onConnect, onDisconnect} = selectedDevice;

  const [isProcessing, setIsProcessing] = useState(false);

  let deviceHandle = {
    icon: 'link',
    onPress: () => {
      setIsProcessing(true);

      onConnect().then(() => setIsProcessing(false));
    },
  };

  if (selectedDevice.isConnected) {
    deviceHandle = {
      icon: 'unlink',
      onPress: () => {
        setIsProcessing(true);
        onDisconnect().then(() => setIsProcessing(false));
      },
    };
  }

  return (
    <Header>
      <Left>
        <TouchableOpacity>{leftButton}</TouchableOpacity>
      </Left>
      <Body>
        <Title>{title}</Title>
      </Body>
      <Right>
        {selectedDevice.isConnected ? (
          <Button transparent>
            <BatteryIconComponent
              levelPercent={selectedDevice.sensors.battery}
              isCharges={selectedDevice.isCharges}
              isChargingEnable={selectedDevice.isChargingEnable}
            />
          </Button>
        ) : null}

        {isProcessing ? (
          <Button transparent>
            <Spinner color="gray" />
          </Button>
        ) : (
          <TouchableOpacity>
            <Button transparent onPress={deviceHandle.onPress}>
              <Icon name={deviceHandle.icon} type={typeIcon} />
            </Button>
          </TouchableOpacity>
        )}
      </Right>
    </Header>
  );
};

export {HeaderComponent};

HeaderComponent.propTypes = {
  leftButton: PropTypes.element,
  title: PropTypes.string,
};
