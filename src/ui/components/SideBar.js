import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  Container,
  Content,
  Text,
  ListItem,
  Icon,
  Left,
  Body,
} from 'native-base';

const SideBarComponent = ({navigation, typeIcon}) => {
  const createSideBarItem = (navigationNameView, iconName, textItem) => (
    <TouchableOpacity>
      <ListItem icon onPress={() => navigation.navigate(navigationNameView)}>
        <Left>
          <Icon name={iconName} type={typeIcon} style={styles.icons_size} />
        </Left>
        <Body>
          <Text>{textItem}</Text>
        </Body>
      </ListItem>
    </TouchableOpacity>
  );

  return (
    <Container>
      <Content>
        {createSideBarItem('Home', 'heartbeat', 'Монитор')}
        {createSideBarItem('Settings', 'gears', 'Настройки')}
        {createSideBarItem('Notifications', 'bullhorn', 'Журнал ошибок')}
      </Content>
    </Container>
  );
};

export {SideBarComponent};

const styles = StyleSheet.create({
  icons_size: {
    fontSize: 20,
  },
});

SideBarComponent.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};
