import React from 'react';
import PropTypes from 'prop-types';
import Svg, {G, Path, Rect} from 'react-native-svg';

const maxHeightBatteryLevel = 52;
const minHeightBatteryLevel = 4;

const stepPercent = (maxHeightBatteryLevel - minHeightBatteryLevel) / 100;
const batteryLevelColor = [
  {color: 'red', min: 0, max: 19},
  {color: 'yellow', min: 20, max: 59},
  {color: 'green', min: 60, max: 100},
];

const defaultBatteryStyle = {fontSize: 24};

const BatteryIconComponent = ({
  levelPercent,
  isCharges,
  isChargingEnable,
  style = defaultBatteryStyle,
}) => {
  let value = minHeightBatteryLevel + levelPercent * stepPercent;
  let color = batteryLevelColor.find((item) => {
    if (item.min <= levelPercent && item.max >= levelPercent) {
      return item;
    }
  });

  return (
    <Svg
      width={style.fontSize}
      height={style.fontSize}
      viewBox="0 0 43.1555 60">
      <G transform="translate(8.95848 -233.849)">
        <Path
          fill="#ccc"
          strokeWidth="1"
          d="m 30.7331 237.849 h -6.53601 v -3.99999 H 12.197 v 3.99999 H 5.66108 c -1.91002 0 -3.46401 1.55399 -3.46401 3.46401 v 49.072 c 0 1.91002 1.55399 3.46401 3.46401 3.46401 H 30.7321 c 1.91096 0 3.46499 -1.55399 3.46499 -3.46401 v -49.072 c 0 -1.91002 -1.55403 -3.46401 -3.46401 -3.46401 Z"
        />
        <Rect
          transform="scale(-1)"
          id="batteryLevel"
          y="-291.4823"
          x="-31.234768"
          width="26.075403"
          height={value}
          fill={color.color}
          ry="2.7779195"
          opacity="1"
        />

        {isCharges && (
          <Path
            fill="#fff"
            stroke="#fff"
            strokeWidth="2.60142"
            d="m 11.7745 291.366 c -0.188124 0 -0.381011 -0.054 -0.55723 -0.16483 c -0.511984 -0.32397 -0.754879 -1.04297 -0.571517 -1.70228 l 4.98649 -18.0259 H 6.29031 c -0.442926 0 -0.847752 -0.29271 -1.05493 -0.76162 c -0.204794 -0.46891 -0.176218 -1.03444 0.076202 -1.46924 L 21.8046 240.823 c 0.32386 -0.557 0.940623 -0.75593 1.45499 -0.49448 c 0.52151 0.27282 0.80965 0.94634 0.688203 1.60849 l -3.35291 18.168 h 9.50863 c 0.452451 0 0.866802 0.30692 1.06921 0.79288 c 0.200032 0.48596 0.150024 1.0657 -0.12621 1.49482 l -18.3291 28.4186 c -0.230989 0.36092 -0.583425 0.55417 -0.943005 0.55417 Z M 8.56447 268.631 h 8.71565 c 0.383394 0 0.740592 0.21882 0.964437 0.58826 c 0.223844 0.36945 0.285759 0.84404 0.164311 1.27884 l -3.31004 11.9614 l 12.5853 -19.5122 h -8.57039 c -0.35958 0 -0.70249 -0.19609 -0.928716 -0.53143 c -0.226226 -0.33534 -0.311954 -0.77583 -0.235751 -1.19642 l 2.3718 -12.8481 Z"
          />
        )}

        {isChargingEnable && (
          <G>
            <Path
              fill="#fff"
              d="m -5.6576819,278.02838 c -5.2120311,0 -9.4506471,-5.58655 -9.4506471,-12.45608 v -3.11402 c 0,-1.71686 1.060048,-3.11402 2.362661,-3.11402 H 1.4303009 c 1.3026121,0 2.3626609,1.39716 2.3626609,3.11402 v 3.11402 c 0,6.86953 -4.23861528,12.45608 -9.4506437,12.45608 z m -7.0879861,-16.60811 c -0.433154,0 -0.787553,0.46502 -0.787553,1.03801 v 3.11402 c 0,5.72358 3.532967,10.38007 7.8755391,10.38007 4.3425692,0 7.8755348,-4.65649 7.8755348,-10.38007 v -3.11402 c 0,-0.57299 -0.354399,-1.03801 -0.787552,-1.03801 z"
            />
            <Path
              fill="#fff"
              d="m -9.5954515,261.42027 c -0.4347295,0 -0.7875555,-0.46502 -0.7875555,-1.038 v -8.30406 c 0,-0.57298 0.352826,-1.038 0.7875555,-1.038 0.434731,0 0.7875535,0.46502 0.7875535,1.038 v 8.30406 c 0,0.57298 -0.3528225,1.038 -0.7875535,1.038 z"
            />
            <Path
              fill="#fff"
              d="m -1.7199153,261.42027 c -0.4347296,0 -0.7875521,-0.46502 -0.7875521,-1.038 v -8.30406 c 0,-0.57298 0.3528225,-1.038 0.7875521,-1.038 0.4347312,0 0.78755526,0.46502 0.78755526,1.038 v 8.30406 c 0,0.57298 -0.35282406,1.038 -0.78755526,1.038 z"
            />
            <Path
              fill="#fff"
              d="m -5.6576819,292.62193 c -0.4347312,0 -0.7875536,-0.7468 -0.7875536,-1.66695 v -13.33565 c 0,-0.92016 0.3528224,-1.66696 0.7875536,-1.66696 0.4347297,0 0.7875538,0.7468 0.7875538,1.66696 v 13.33565 c 0,0.92015 -0.3528241,1.66695 -0.7875538,1.66695 z"
            />
          </G>
        )}
      </G>
    </Svg>
  );
};

export {BatteryIconComponent};

BatteryIconComponent.propTypes = {
  levelPercent: PropTypes.number.isRequired,
};
