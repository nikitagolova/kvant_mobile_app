import React, {useContext} from 'react';
import PropTypes from 'prop-types';

import {StyleSheet} from 'react-native';
import {
  Container,
  Text,
  Button,
  Content,
  ListItem,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';
import {Context} from '../../../helpers/index';
import {HeaderComponent} from '../../components/index';

const SettingsView = ({drawerNavigation, stackNavigation}) => {
  const {selectedDevice, typeIcon} = useContext(Context);

  const createSettingsMenuItem = (
    navigationViewName,
    iconName,
    menuItemText,
    subMenuItemText = '',
  ) => (
    <ListItem
      thumbnail
      onPress={() => stackNavigation.navigate(navigationViewName)}>
      <Left>
        <Icon name={iconName} type={typeIcon} />
      </Left>
      <Body>
        <Text>{menuItemText}</Text>
        {subMenuItemText != '' && (
          <Text style={styles.subMenuItem}>{subMenuItemText}</Text>
        )}
      </Body>
      <Right>
        <Icon name="chevron-right" type={typeIcon} />
      </Right>
    </ListItem>
  );

  return (
    <Container>
      <HeaderComponent
        title="НАСТРОЙКИ"
        leftButton={
          <Button transparent onPress={() => drawerNavigation.navigate('Home')}>
            <Icon name="arrow-left" type={typeIcon} />
          </Button>
        }
      />

      <Content>
        <ListItem itemDivider>
          <Text>Настройки Bluetooth</Text>
        </ListItem>

        {createSettingsMenuItem(
          'Settings_ScanDevices',
          'bluetooth',
          'Базовый модуль',
          selectedDevice.name,
        )}

        <ListItem itemDivider>
          <Text>Об устройстве</Text>
        </ListItem>

        {createSettingsMenuItem(
          'Settings_AboutDevice',
          'question',
          'Общая информация',
        )}
      </Content>
    </Container>
  );
};

export {SettingsView};

const styles = StyleSheet.create({
  scrollView: {
    height: 150,
    width: 200,
    marginBottom: 10,
  },
  subMenuItem: {
    color: '#acafb5',
  },
});

SettingsView.propTypes = {
  drawerNavigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  stackNavigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};
