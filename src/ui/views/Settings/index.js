import React from 'react';
import {SettingsView} from './SettingsView';
import {AboutDeviceView} from './AboutDeviceView';
import {ScanDevicesView} from './ScanDevicesView';

import {createStackNavigator} from '@react-navigation/stack';

const SettingsNavigation = ({drawerNavigation}) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator initialRouteName="Settings_Home" headerMode="none">
      <Stack.Screen name="Settings_Home">
        {(props) => (
          <SettingsView
            stackNavigation={props.navigation}
            drawerNavigation={drawerNavigation}
          />
        )}
      </Stack.Screen>
      <Stack.Screen name="Settings_AboutDevice">
        {(props) => <AboutDeviceView {...props} />}
      </Stack.Screen>
      <Stack.Screen name="Settings_ScanDevices">
        {(props) => <ScanDevicesView {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export {SettingsNavigation};
