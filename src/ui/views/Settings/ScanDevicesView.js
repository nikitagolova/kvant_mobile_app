import React, {useState, useContext, useEffect} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {HeaderComponent} from '../../components/index';
import {Context, BleHelper} from '../../../helpers/index';
import {
  Container,
  Button,
  ListItem,
  Left,
  Body,
  Text,
  Icon,
  Grid,
  Col,
  Row,
} from 'native-base';

const ScanDevicesView = ({navigation}) => {
  const [devices, setDevices] = useState([]);
  const [isScanDevices, setIsScanDevices] = useState(false);

  const {selectedDevice, typeIcon} = useContext(Context);

  const startScanDevices = () => {
    setIsScanDevices(true);

    BleHelper.startScanDevices((device) => {
      setDevices((devices) => {
        let findDevice = devices.find((item) => item.id === device.id);
        if (findDevice) {
          return devices;
        }

        return [...devices, device];
      });
    });
  };

  const stopScanDevices = () => {
    setDevices([]);
    setIsScanDevices(false);

    BleHelper.stopScanDevices();
  };

  const onSelectedDevice = (device) => {
    BleHelper.stopScanDevices();

    if (selectedDevice.isConnected) {
      selectedDevice.onDisconnect();
    }

    selectedDevice.onChange(device.name, device.id);
    navigation.navigate('Settings_Home');
  };

  let scanDevicesButton = {
    text: 'Поиск',
    handler: startScanDevices,
  };
  if (isScanDevices) {
    scanDevicesButton = {
      text: 'Остановить поиск',
      handler: stopScanDevices,
    };
  }

  useEffect(() => {
    startScanDevices();
  }, []);

  return (
    <Container>
      <HeaderComponent
        title="ПОИСК"
        leftButton={
          <Button
            transparent
            onPress={() => navigation.navigate('Settings_Home')}>
            <Icon name="arrow-left" type={typeIcon} />
          </Button>
        }
      />

      <Grid>
        <Col>
          <ScrollView>
            {devices.map((device, key) => (
              <ListItem
                key={'device-' + key}
                thumbnail
                onPress={() => onSelectedDevice(device)}>
                <Left>
                  <Icon name="bluetooth" type={typeIcon} />
                </Left>
                <Body>
                  <Text>{!device.name ? 'No name' : device.name}</Text>
                </Body>
              </ListItem>
            ))}
          </ScrollView>
        </Col>

        <Row style={styles.bottom}>
          <Button onPress={scanDevicesButton.handler} transparent>
            <Text>{scanDevicesButton.text}</Text>
          </Button>
        </Row>
      </Grid>
    </Container>
  );
};

export {ScanDevicesView};

const styles = StyleSheet.create({
  bottom: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginBottom: 10,
    height: 50,
  },
});
