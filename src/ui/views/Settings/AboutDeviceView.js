import React, {useContext} from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Button,
  Icon,
  Body,
  Text,
  ListItem,
  Right,
  Grid,
  Row,
} from 'native-base';
import {Context} from '../../../helpers/index';
import {HeaderComponent} from '../../components/index';

const AboutDeviceView = ({navigation}) => {
  const {selectedDevice, errors, typeIcon} = useContext(Context);
  const {sensors} = selectedDevice;

  const aboutDeviceInfoItem = (name, value) => {
    return (
      <ListItem>
        <Body>
          <Text>{name}</Text>
        </Body>
        <Right>
          <Text>{value} </Text>
        </Right>
      </ListItem>
    );
  };

  let recordStatus = 'Запись не идет';
  if (selectedDevice.isRecordingProgress) {
    recordStatus = 'Идет запись';
  }

  let batteryValue = '';
  let batteryStatus = 'Ошибка';

  if (!errors.value.battery.length) {
    batteryValue = sensors.battery + ' %';
    batteryStatus = 'Не заряжается';

    if (selectedDevice.isChargingEnable) {
      if (selectedDevice.isCharges) {
        setBatteryStatus('Заряжена');
      } else {
        setBatteryStatus('Заряжается');
      }
    }
  }

  let cardValue = '';

  if (!errors.value.card.length) {
    cardValue = sensors.card + ' Мб';
  }

  return (
    <Container>
      <HeaderComponent
        title="ОБ УСТРОЙСТВЕ"
        leftButton={
          <Button
            transparent
            onPress={() => navigation.navigate('Settings_Home')}>
            <Icon name="arrow-left" type={typeIcon} />
          </Button>
        }
      />

      {selectedDevice.isConnected ? (
        <React.Fragment>
          {aboutDeviceInfoItem('Состояние записи', recordStatus)}
          {aboutDeviceInfoItem('Состояние батареи', batteryStatus)}
          {aboutDeviceInfoItem('Уровень заряда батареи', batteryValue)}
          {aboutDeviceInfoItem('Свободно памяти', cardValue)}
        </React.Fragment>
      ) : (
        <Grid>
          <Row style={styles.center_content}>
            <Text>Устройство не подключено!</Text>
          </Row>
        </Grid>
      )}
    </Container>
  );
};

export {AboutDeviceView};

const styles = StyleSheet.create({
  center_content: {
    alignContent: 'center',
    justifyContent: 'center',
  },
});
