import React, {useContext} from 'react';
import PropTypes from 'prop-types';

import {StyleSheet} from 'react-native';
import {
  Container,
  Icon,
  Button,
  Grid,
  Row,
  Col,
  Text,
  Content,
} from 'native-base';
import {HeaderComponent} from '../components/index';
import {Context} from '../../helpers/index';

const HomeView = ({navigation}) => {
  const {selectedDevice, typeIcon} = useContext(Context);
  const {sensors} = selectedDevice;

  let startButtonColor = '#009900';
  let stopButtonColor = '#9c9c9c';
  if (selectedDevice.isRecordingProgress) {
    startButtonColor = '#9c9c9c';
    stopButtonColor = '#990000';
  }

  return (
    <Container>
      <HeaderComponent
        title={selectedDevice.name}
        leftButton={
          <Button transparent onPress={() => navigation.openDrawer()}>
            <Icon name="navicon" type={typeIcon} />
          </Button>
        }
      />
      <Grid>
        <Row>
          <Col>
            <Row>
              <Content>
                <Text style={{...styles.info_text, fontSize: 50}}>
                  {sensors.heartRate}
                </Text>
                <Text style={styles.info_text}>уд/мин</Text>
              </Content>
            </Row>
          </Col>
          <Col>
            <Icon
              name="heartbeat"
              type={typeIcon}
              style={{...styles.info_icon, color: 'red'}}
            />
          </Col>
        </Row>

        <Row style={styles.bottom}>
          <Col style={styles.center_content}>
            <Button
              transparent
              onPress={() => {
                selectedDevice.onCommunicate('Start');
              }}>
              <Icon
                name="play-circle-o"
                type={typeIcon}
                style={{...styles.nav_icons, color: startButtonColor}}
              />
            </Button>
          </Col>

          <Col style={styles.center_content}>
            <Button
              transparent
              onPress={() => selectedDevice.onCommunicate('Stop')}>
              <Icon
                name="stop-circle-o"
                type={typeIcon}
                style={{...styles.nav_icons, color: stopButtonColor}}
              />
            </Button>
          </Col>
        </Row>
      </Grid>
    </Container>
  );
};

export {HomeView};

const styles = StyleSheet.create({
  bottom: {
    alignItems: 'flex-end',
    marginBottom: 20,
  },
  center_content: {
    alignItems: 'center',
  },
  info_icon: {
    fontSize: 150,
  },
  nav_icons: {
    fontSize: 60,
  },
  info_text: {
    fontFamily: 'Arial',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 5,
  },
});

HomeView.propTypes = {
  navigation: PropTypes.shape({
    openDrawer: PropTypes.func,
  }).isRequired,
};
