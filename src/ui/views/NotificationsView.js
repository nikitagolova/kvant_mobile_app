import React, {useContext} from 'react';
import PropTypes from 'prop-types';

import {StyleSheet} from 'react-native';
import {
  Container,
  Button,
  Text,
  Icon,
  ListItem,
  Content,
  List,
  Left,
  Body,
  Grid,
  Row,
} from 'native-base';
import {HeaderComponent} from '../components/index';
import {Context} from '../../helpers/index';

const NotificationsView = ({navigation}) => {
  const {selectedDevice, errors, typeIcon} = useContext(Context);

  const getNotificationsGroup = (groupName, groupErrors, key) => {
    return (
      <React.Fragment>
        <ListItem itemDivider>
          <Text>{groupName}</Text>
        </ListItem>

        {groupErrors.length ? (
          <React.Fragment>
            {groupErrors.map((error, index) => (
              <ListItem key={'notification-error-' + key + '-' + index} icon>
                <Left>
                  <Button transparent>
                    <Icon
                      style={{color: 'red'}}
                      name="warning"
                      type="FontAwesome"
                    />
                  </Button>
                </Left>
                <Body>
                  <Text>{error}</Text>
                </Body>
              </ListItem>
            ))}
          </React.Fragment>
        ) : (
          <ListItem icon>
            <Left>
              <Button transparent>
                <Icon
                  style={{color: 'green'}}
                  name="check-circle"
                  type="FontAwesome"
                />
              </Button>
            </Left>
            <Body>
              <Text>Ошибок не найдено</Text>
            </Body>
          </ListItem>
        )}
      </React.Fragment>
    );
  };

  return (
    <Container>
      <HeaderComponent
        title="ЖУРНАЛ"
        leftButton={
          <Button transparent onPress={() => navigation.navigate('Home')}>
            <Icon name="arrow-left" type={typeIcon} />
          </Button>
        }
      />

      {selectedDevice.isConnected ? (
        <Content>
          <List>
            {getNotificationsGroup('Ошибки ЧСС', errors.value.heartRate, 'hr')}
            {getNotificationsGroup(
              'Ошибки батареи',
              errors.value.battery,
              'batt',
            )}
            {getNotificationsGroup(
              'Ошибки SD карты',
              errors.value.card,
              'card',
            )}
            {getNotificationsGroup(
              'Ошибки начала записи',
              errors.value.start,
              'start',
            )}
            {getNotificationsGroup(
              'Ошибки окончания записи',
              errors.value.stop,
              'stop',
            )}
          </List>
        </Content>
      ) : (
        <Grid>
          <Row style={styles.center_content}>
            <Text>Устройство не подключено!</Text>
          </Row>
        </Grid>
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  center_content: {
    alignContent: 'center',
    justifyContent: 'center',
  },
});

export {NotificationsView};

NotificationsView.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};
