import React from 'react';
import {StyleSheet} from 'react-native';
import {Text, Container, Grid, Row} from 'native-base';

const PreviewView = (props) => {
  return (
    <Container>
      <Grid>
        <Row style={styles.center_content}>
          <Text>Здесь могла бы быть ваша реклама!</Text>
        </Row>
      </Grid>
    </Container>
  );
};

export {PreviewView};

const styles = StyleSheet.create({
  center_content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
