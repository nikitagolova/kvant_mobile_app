import {useState, useEffect} from 'react';

const useBluetoothCommandTimer = (onTickCallback, tickValue = 1000) => {
  const [timerId, setTimerId] = useState(undefined);
  const [isTimerEnabled, setIsTimerEnabled] = useState(false);

  useEffect(() => {
    if (isTimerEnabled) {
      clearTimeout(timerId);
      onTickCallback();

      let timerIdTemp = setTimeout(onTickTimer, tickValue);
      setTimerId(timerIdTemp);
    } else {
      clearTimeout(timerId);
    }
  }, [isTimerEnabled]);

  const start = () => {
    setIsTimerEnabled(true);
  };

  const stop = () => {
    setIsTimerEnabled(false);
  };

  const onTickTimer = () => {
    clearTimeout(timerId);
    onTickCallback();

    if (!isTimerEnabled) {
      return;
    }

    let timerIdTemp = setTimeout(onTickTimer, tickValue);
    setTimerId(timerIdTemp);
  };

  return {
    value: timerId,
    start,
    stop,
  };
};

export {useBluetoothCommandTimer};
