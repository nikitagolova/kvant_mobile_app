import {useState} from 'react';
import {
  BleHelper,
  requestCommandsEnum,
  responceCommandsEnum,
} from '../helpers/index';
import {useBluetoothCommandTimer} from '../hooks/useTimer';

const sensorsEnum = {
  battery: 'battery',
  heartRate: 'heartRate',
  card: 'card',
};

export const useSelectedDevice = (
  defaultName = '',
  defaultId = '',
  defaultError = null,
) => {
  const errors = defaultError;

  // Имя устройства.
  const [name, setName] = useState(defaultName);

  //Id устройства.
  const [id, setId] = useState(defaultId);

  // Указатель, что устройство подключено.
  const [isConnected, setIsConnected] = useState(false);

  // Указатель, что устройство сейчас записывает показания ЭКГ.
  const [isRecordingProgress, setIsRecordingProgress] = useState(false);

  // Указатель, что к устройству подключена зарядка.
  const [isChargingEnable, setIsChargingEnable] = useState(false);

  // Указатель, что устройство заряжается.
  const [isCharges, setIsCharges] = useState(false);

  // Показания датчиков устройства.
  const [sensors, setSensors] = useState({
    battery: 0,
    heartRate: 0,
    card: 0,
  });

  // Подключение к устройству
  const onConnect = async () => {
    // Устройство не подключено.
    if (!id) {
      let errorMessage = 'Устройство не выбрано!';
      console.log('On connect', errorMessage);

      errors.showMessageBox(errorMessage);
      return;
    }

    // Подключение к устройству.
    await BleHelper.deviceConnect(
      id,
      _connectedCallback,
      _disconnectedCallback,
    );
  };

  const onDisconnect = async () => {
    // Устройство не подключено.
    if (!id) {
      let errorMessage = 'Устройство не выбрано!';
      console.log('On disconnect', errorMessage);

      errors.showMessageBox(errorMessage);
      return;
    }

    // Отключение от устройства.
    await BleHelper.deviceDisconnect(id);
  };

  // Отправка запросов старт или стоп
  const onCommunicate = async (value) => {
    if (!isConnected) {
      let errorMessage = 'Устройство не выбрано!';
      console.log('On communicate', errorMessage);

      errors.showMessageBox(errorMessage);
      return;
    }

    // Для избежания отправки повторного запроса.
    if (
      !isConnected ||
      (isRecordingProgress && value == 'Start') ||
      (!isRecordingProgress && value == 'Stop')
    ) {
      return;
    }

    await BleHelper.writeDataToCharacteristic(value, id);
  };

  // Таймер запроса уровня заряда батареи.
  const _getBattTimer = useBluetoothCommandTimer(() => {
    BleHelper.writeDataToCharacteristic(requestCommandsEnum.battery, id);
  }, 60000);

  // Таймер запроса ЧСС.
  const _getHrTimer = useBluetoothCommandTimer(() => {
    BleHelper.writeDataToCharacteristic(requestCommandsEnum.heartRate, id);
  }, 5000);

  // Таймер запроса статуса устройства.
  const _getStatusTimer = useBluetoothCommandTimer(() => {
    BleHelper.writeDataToCharacteristic(requestCommandsEnum.status, id);
  }, 15000);

  // Таймер запроса оставшегося объема памяти на SD карте.
  const _getCardTimer = useBluetoothCommandTimer(() => {
    BleHelper.writeDataToCharacteristic(requestCommandsEnum.card, id);
  }, 60000);

  // Отправка запроса синхронизации времени устройства.
  const _sendSetTimeRequest = () => {
    BleHelper.writeDataToCharacteristic(
      requestCommandsEnum.time + _getUnixTime(),
      id,
    );
  };

  // Получение времени в формате Unix epoch с учетом часового пояса.
  const _getUnixTime = () => {
    let currentDate = new Date();
    let timeZoneOffsetInSeconds = currentDate.getTimezoneOffset() * 60;

    let result =
      Math.round(currentDate.getTime() / 1000) - timeZoneOffsetInSeconds;
    return result;
  };

  // Установка значения значений датчиков.
  const _setSensorValue = (sensorType, value) => {
    const tempSensors = sensors;
    tempSensors[sensorType] = value;

    setSensors({
      battery: tempSensors.battery,
      heartRate: tempSensors.heartRate,
      card: tempSensors.card,
    });
  };

  const _connectedCallback = async (errorMessage) => {
    if (errorMessage) {
      console.log('Connected callback', errorMessage);

      errors.showMessageBox(errorMessage);
      return;
    }

    BleHelper.subscribeMonitorCharacteristic(id, _responceCallback);

    setIsConnected(true);

    // Синхроизация времени устройства.
    await _sendSetTimeRequest();

    // Запуск таймеров, которые опрашивают устройство.
    _getStatusTimer.start();
    _getBattTimer.start();
    _getHrTimer.start();
    _getCardTimer.start();
  };

  const _responceCallback = (responce) => {
    let responceErrors = [];

    responce.data.map((answer) => {
      // Вывод в консоль пришедшей команды.
      console.log(
        'Command: ',
        responce.command,
        'key: ',
        answer.key,
        'message: ',
        answer.message,
        'value: ',
        answer.value,
      );

      if (answer.key != '00') {
        if (responce.command == responceCommandsEnum.status) {
          let isChecked = answer.value != 0;

          switch (answer.key) {
            case '01':
              if (!isRecordingProgress && isChecked) {
                setIsRecordingProgress(true);
              } else if (isRecordingProgress && !isChecked) {
                setIsRecordingProgress(false);
              }
              break;
            case '02':
              setIsChargingEnable(isChecked);
              break;
            case '04':
              setIsCharges(isChecked);
              break;
          }
          return;
        }

        if (
          responce.command == responceCommandsEnum.start ||
          responce.command == responceCommandsEnum.stop
        ) {
          errors.showMessageBox(answer.message);
        }

        responceErrors.push(answer.message);
        return;
      }

      switch (responce.command) {
        case responceCommandsEnum.start:
          setIsRecordingProgress(true);
          break;
        case responceCommandsEnum.stop:
          setIsRecordingProgress(false);
          break;
        case responceCommandsEnum.battery:
          _setSensorValue(sensorsEnum.battery, answer.value);
          break;
        case responceCommandsEnum.heartRate:
          _setSensorValue(sensorsEnum.heartRate, answer.value);
          break;
        case responceCommandsEnum.card:
          _setSensorValue(sensorsEnum.card, answer.value);
          break;
      }
    });

    errors.setErrors(responce.command, responceErrors);
  };

  const _disconnectedCallback = (errorMessage) => {
    if (errorMessage) {
      console.log('Disconnected callback', errorMessage);

      errors.showMessageBox(errorMessage);
    }

    BleHelper.unsubscribeMonitorCharacteristic();

    // Остановка таймеров запросов статуса, заряда батареи и значения ЧСС.
    _getStatusTimer.stop();
    _getBattTimer.stop();
    _getHrTimer.stop();
    _getCardTimer.stop();

    setIsConnected(false);
  };

  return {
    name,
    id,
    sensors,
    isConnected,
    isRecordingProgress,
    isCharges,
    isChargingEnable,

    onChange: (name, id) => {
      setName(name);
      setId(id);
    },
    onConnect,
    onDisconnect,
    onCommunicate,
  };
};
