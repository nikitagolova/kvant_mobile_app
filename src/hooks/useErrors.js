import {useState, useEffect} from 'react';
import {Alert} from 'react-native';
import {responceCommandsEnum} from '../helpers/index';

const useErrors = () => {
  const [value, setValue] = useState({
    start: [],
    stop: [],
    battery: [],
    card: [],
    heartRate: [],
  });

  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    if (errorMessage) {
      Alert.alert(errorMessage, '', [{text: 'OK', onPress: () => {}}], {
        onDismiss: () => {},
        cancelable: true,
      });

      setErrorMessage('');
    }
  }, [errorMessage]);

  const setErrors = (nameRequest, errors) => {
    switch (nameRequest) {
      case responceCommandsEnum.start:
        setValue((value) => {
          value.start = errors;
          return value;
        });
        break;
      case responceCommandsEnum.stop:
        setValue((value) => {
          value.stop = errors;
          return value;
        });
        break;
      case responceCommandsEnum.battery:
        setValue((value) => {
          value.battery = errors;
          return value;
        });
        break;
      case responceCommandsEnum.heartRate:
        setValue((value) => {
          value.heartRate = errors;
          return value;
        });
        break;
      case responceCommandsEnum.card:
        setValue((value) => {
          value.card = errors;
          return value;
        });
        break;
    }
  };

  return {
    value,

    showMessageBox: (message) => {
      setErrorMessage(message);
    },
    setErrors,
  };
};

export {useErrors};
