import 'react-native-gesture-handler';

import React, {useState, useEffect} from 'react';
import {AsyncStorage} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {
  HomeView,
  PreviewView,
  SettingsNavigation,
  NotificationsView,
} from './ui/views/index';
import {Context, BleHelper} from './helpers/index';
import {useSelectedDevice, useErrors} from './hooks/index';
import {SideBarComponent} from './ui/components/index';

export default function App() {
  const errors = useErrors();
  const selectedDevice = useSelectedDevice('', '', errors);

  const [isMainViewVisible, setIsMainViewVisible] = useState(false);

  const Drawer = createDrawerNavigator();
  const typeIcon = 'FontAwesome';

  useEffect(() => {
    AsyncStorage.getItem('curentDevice', (error, result) => {
      if (error) {
        console.log('Load selected device', error.message);
        return;
      }

      let device = JSON.parse(result);

      selectedDevice.onChange(device.name, device.id);
      console.log('Load selected device sucsess', device);
    });

    BleHelper.enableBluetoothModule(setIsMainViewVisible);
  }, []);

  useEffect(() => {
    let device = {
      name: selectedDevice.name,
      id: selectedDevice.id,
    };

    AsyncStorage.setItem('curentDevice', JSON.stringify(device), (error) => {
      if (error) {
        console.log('Save selected device', error.message);
        return;
      }

      console.log('Save selected device sucsess');
    });
  }, [selectedDevice.id]);

  if (!isMainViewVisible) {
    return <PreviewView />;
  } else {
    return (
      <Context.Provider
        value={{
          typeIcon,
          selectedDevice,
          errors,
        }}>
        <NavigationContainer>
          <Drawer.Navigator
            drawerContent={(props) => (
              <SideBarComponent {...props} typeIcon={typeIcon} />
            )}>
            <Drawer.Screen name="Home">
              {(props) => <HomeView {...props} />}
            </Drawer.Screen>
            <Drawer.Screen name="Settings">
              {(props) => (
                <SettingsNavigation drawerNavigation={props.navigation} />
              )}
            </Drawer.Screen>
            <Drawer.Screen name="Notifications">
              {(props) => <NotificationsView {...props} />}
            </Drawer.Screen>
          </Drawer.Navigator>
        </NavigationContainer>
      </Context.Provider>
    );
  }
}
